$(document).ready(function(){

    $('.c-homeContent .owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        smartSpeed: 450,
        dots: false,
        nav: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            1200: {
                items: 2
            }
        }
    });

    $('.c-innerPage .owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        smartSpeed: 450,
        dots: false,
        nav: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });

    // filter --- [[
    $('#filter li a').click(function() {
        var filterClass = $(this).attr('class');

        $('#filter li').removeClass('active');
        $(this).parent().addClass('active');

        if(filterClass == 'all') {
          $('#tab-holder').children('div.item').show();
        }
        else {
          $('#tab-holder').children('div:not(.' + filterClass + ')').hide();
          $('#tab-holder').children('div.' + filterClass).show();
        }
        return false;
    });
    // filter --- ]]

    // filter all-games--- [[
    $('#filter__allGames li a').click(function() {
        var filterClassAllGames = $(this).attr('class');

        $('#filter__allGames li').removeClass('active');
        $(this).parent().addClass('active');

        if(filterClassAllGames == 'all') {
            $('#tab-holder__allGames').children('div.item').show();
        }
        else {
            $('#tab-holder__allGames').children('div:not(.' + filterClassAllGames + ')').hide();
            $('#tab-holder__allGames').children('div.' + filterClassAllGames).show();
        }
        return false;
    });
    // filter all-games --- ]]
  });